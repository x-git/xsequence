package com.xuanner.seq;

import com.xuanner.seq.sequence.Sequence;
import com.xuanner.seq.sequence.impl.SnowflakeSequence;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by xuan on 2018/5/9.
 */
public class SnowflakeTest_Api {

    private Sequence userSeq;

    @Before
    public void setup() {

        SnowflakeSequence snowflakeSequence = new SnowflakeSequence();
        snowflakeSequence.setDatacenterId(1);
        snowflakeSequence.setWorkerId(2);
        userSeq = snowflakeSequence;
    }

    @Test
    public void test() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            System.out.println("++++++++++id:" + userSeq.nextValue());
        }
        System.out.println("interval time:" + (System.currentTimeMillis() - start));
    }
}
