package com.xuanner.seq.range;

import java.util.concurrent.atomic.AtomicLong;

/**
 * 区间模型
 * Created by xuan on 2018/1/10.
 */
public class SeqRange {

    /**
     * 区间开始
     */
    private final long       min;
    /**
     * 区间结束
     */
    private final long       max;
    /**
     * 当前值
     */
    private final AtomicLong value;
    /**
     * 区间值是否耗尽，每次耗尽就会去重新获取一个新的区间
     */
    private volatile boolean over = false;

    public SeqRange(long min, long max) {
        this.min = min;
        this.max = max;
        this.value = new AtomicLong(min);
    }

    public long getBatch(int size) {
        long currentValue = value.getAndAdd(size) + size - 1;
        if (currentValue > max) {
            over = true;
            return -1;
        }

        return currentValue;
    }

    public long getAndIncrement() {
        long currentValue = value.getAndIncrement();
        if (currentValue > max) {
            over = true;
            return -1;
        }

        return currentValue;
    }

    public long getMin() {
        return min;
    }

    public long getMax() {
        return max;
    }

    public boolean isOver() {
        return over;
    }

    public void setOver(boolean over) {
        this.over = over;
    }

    @Override
    public String toString() {
        return "max: " + max + ", min: " + min + ", value: " + value;
    }

}
